/** Homework - 05 Еклашева
 * Реализовать программу на Java, которая для последовательности чисел, оканчивающихся на -1 выведет самую минимальную
 * цифру, встречающуюся среди чисел последовательности
 */
package com.company;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);
        int min = 9;
        int nn = sc.nextInt() ;
        int a;
        while (nn != -1)
       {
            do {
                a = nn % 10;
                nn = nn / 10;
                if (a < min) { min = a; }
                }
            while (nn > 0);
            nn = sc.nextInt();
       }
        System.out.println(min);
    }
}
