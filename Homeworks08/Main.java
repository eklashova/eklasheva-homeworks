/**
 * На вход подается информация о людях в количестве 10 человек (имя - строка, вес - вещественное число).
 *
 * Считать эти данные в массив объектов.
 *
 * Вывести в отсортированном по возрастанию веса порядке.
 */
package com.company;

import java.util.Scanner;


public class Main {

    public static void main(String[] args) {

        int n = 10;
        Scanner sc = new Scanner(System.in);
	    Person[] persons = new Person[n];
        for (int i = 0; i < n; i++) {
            persons[i] = new Person();
            System.out.println("Введите имя "+ (i + 1)+ " человека: ");
            persons[i].setName(sc.next());
            System.out.println("Введите вес "+ (i + 1) + " человека: ");
            persons[i].setWeight(sc.nextDouble());
        }
        for (int i = 0; i < n; i++) {
            double min = persons[i].getWeight();
            int min_index = i;
            for (int j = i + 1; j < n; j++) {
                if (persons[j].getWeight() < min) {
                    min = persons[j].getWeight();
                    min_index = j;
                }
            }
            double weight_1 = persons[i].getWeight();
            String name_1 = persons[i].getName();
            double weight_2 = persons[min_index].getWeight();
            String name_2 = persons[min_index].getName();
            persons[i].setWeight(weight_2);
            persons[i].setName(name_2);
            persons[min_index].setWeight(weight_1);
            persons[min_index].setName(name_1);
        }

        for (int i = 0; i < n; i++) {
            System.out.println(persons[i].getName()+" "+persons[i].getWeight());

        }
        }
    }

