package com.company;

public class Person {

    private String name;

    private double weight;

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(double weight) {
        if (weight < 0 || weight > 500) {
            weight = 0;
        }
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    public String getName() {
        return name;
    }
}
